GET http://localhost:5000/datas

###
GET http://localhost:5000/datas/_id

###
POST http://localhost:5000/datas
Content-Type: application/json

  {
    "indonesia": "Dompet",
    "english": "Wallet",
    "image": "https://images.unsplash.com/photo-1579014134953-1580d7f123f3?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469"
  }

// --------------------------- DATA YANG LAIN --------------------------- 
###
POST http://localhost:5000/datas
Content-Type: application/json

  {
    "indonesia": "Apel",
    "english": "Apple",
    "image": "https://images.unsplash.com/photo-1560806887-1e4cd0b6cbd6?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074"
  }

###
POST http://localhost:5000/datas
Content-Type: application/json

  {
    "indonesia": "Pir",
    "english": "Pear",
    "image": "https://images.unsplash.com/photo-1514756331096-242fdeb70d4a?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470"
  }

###
POST http://localhost:5000/datas
Content-Type: application/json

  {
    "indonesia": "Tas",
    "english": "Bag",
    "image":"https://images.unsplash.com/photo-1566150905458-1bf1fc113f0d?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1471"
  }

###
POST http://localhost:5000/datas
Content-Type: application/json

  {
    "indonesia": "Cangkir",
    "english": "Mug",
    "image":"https://images.unsplash.com/photo-1616241673347-67fb5dfa3167?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=728"
  }

###
POST http://localhost:5000/datas
Content-Type: application/json

  {
    "indonesia": "Buku",
    "english": "Book",
    "image":"https://images.unsplash.com/photo-1621827979802-6d778e161b28?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735"
  }
// -------------------------------------------------------------------------------------------------------

###
PATCH http://localhost:5000/datas/_id
Content-Type: application/json

{
    "indonesia": "Pir",
    "english": "Pear",
    "image": "https://images.unsplash.com/photo-1514756331096-242fdeb70d4a?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470"
}

###
DELETE http://localhost:5000/datas/_id