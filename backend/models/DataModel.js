import mongoose from "mongoose";
 
const Data = mongoose.Schema({
    indonesia:{
        type: String,
        required: true
    },
    english:{
        type: String,
        required: true
    },
    image:{
        type: String,
        required: true
    }
});
 
export default mongoose.model('Datas', Data);