import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import DataRoute from "./routes/DataRoute.js";
 
const app = express();
// CARA LOCALHOST
mongoose.connect('mongodb://127.0.0.1:27017/fullstack_db',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// CARA DOCKER
/*
mongoose.connect('mongodb://mongo:27017/fullstack_db',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
*/

const db = mongoose.connection;
db.on('error', (error) => console.log(error));
db.once('open', () => console.log('Database Connected...'));
 
app.use(cors());
app.use(express.json());
app.use(DataRoute);
 
app.listen(5000, ()=> console.log('Server up and running...'));