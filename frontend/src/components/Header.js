import React, {useState, useEffect} from 'react';
import {Text, View, StatusBar} from 'react-native';

const Header = props => {
  return (
    <View
      style={{
        backgroundColor: '#6a8dff',
        elevation: 2,
        paddingVertical: 14,
        justifyContent: 'center',
        alignItems: 'center',
        // borderBottomRightRadius: 10,
        // borderBottomLeftRadius: 10,
      }}>
      <StatusBar backgroundColor={'#6a8dff'} barStyle="light-content" />
      <Text style={{fontWeight: 'bold', fontSize: 18, color: '#FFFFFF'}}>
        {props.title}
      </Text>
      <Text style={{fontSize: 14, color: '#FFFFFF'}}>{props.description}</Text>
    </View>
  );
};

export default Header;
